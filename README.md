# toolbox-cookiecutter

    toolbox for generating cookiecutter projects


## Usage  

    docker run -it --rm mswinson/toolbox-cookiecutter


## Building  


build image  

    git clone https://bitbucket.org/mswinson-lib/docker-toolbox-cookiecutter toolbox-cookiecutter
    cd toolbox-cookiecutter
    docker-compose run sandbox
    > make clean
    > make build


## Contributing

1. Fork it ( https://bitbucket.org/mswinson-lib/docker-toolbox-cookiecutter/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request
