**07/03/2019**

**v0.1.1

    renames image -> toolbox-cookiecutter
    change bitbucket pipelines to use toolbox-packer
    change docker stack to use toolbox-packer


**08/07/2018**

**v0.1.0**

    adds docker client
    adds git
    adds mercurial support  
    adds make support  
    adds bash support  

