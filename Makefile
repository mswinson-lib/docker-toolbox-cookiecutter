.PHONY: clean build style test deploy

BASEIMAGE=cookiecutter/cookiecutter

REPO=mswinson
NAME=toolbox-cookiecutter
ID=$(REPO)/$(NAME)

clean:
	@for line in `./scripts/dependencies`; \
  do \
		docker rmi $(ID):$$line ; \
  done

build:
	@for line in `./scripts/dependencies`; \
  do \
		packer build -var-file=variables.json -var image_tag=$$line -var base_image_name="$(BASEIMAGE):$$line" template.json ; \
  done

style:
	packer validate -var-file=variables.json template.json

deploy:
	docker login -u $(DOCKER_USER) -p $(DOCKER_PASS)
	@for line in `./scripts/dependencies`; \
  do \
		docker push ${ID}:$$line ; \
  done
